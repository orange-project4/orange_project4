
<?php 
ob_start();

include('includes/connection.php');
?>

<?php
include('includes/headerinner2.php');

?>



<div class="breadcrumb-wrap">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Products</a></li>
            <li class="breadcrumb-item active">My Account</li>
        </ul>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- My Account Start -->
<div class="my-account">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="dashboard-nav" data-toggle="pill" href="#dashboard-tab" role="tab"><i class="fa fa-tachometer-alt"></i>Welcome</a>
                    <a class="nav-link" id="orders-nav" data-toggle="pill" href="#orders-tab" role="tab"><i class="fa fa-shopping-bag"></i>Orders</a>
                    <a class="nav-link" id="account-nav" data-toggle="pill" href="#account-tab" role="tab"><i class="fa fa-user"></i>Account Details</a>
                </div>
            </div>
               
            <div class="col-md-9">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="dashboard-tab" role="tabpanel" aria-labelledby="dashboard-nav">
                        <h4>Welcome</h4>
                        <p>
                            Thanks you for shopping with us , feel free to edit your profile
                            <h1>Greener Team</h1>
                        </p>
                    </div>
                    <div class="tab-pane fade" id="orders-tab" role="tabpanel" aria-labelledby="orders-nav">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                           
                                        <tr>
                                        <th>No.</th>
                                        <th>Product</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><button class="btn btn-secondary">View</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><button class="btn btn-secondary">View</button></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><button class="btn btn-secondary">View</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="payment-tab" role="tabpanel" aria-labelledby="payment-nav">
                        <h4>Payment Method</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In condimentum quam ac mi viverra dictum. In efficitur ipsum diam, at dignissim lorem tempor in. Vivamus tempor hendrerit finibus. Nulla tristique viverra nisl, sit amet bibendum ante suscipit non. Praesent in faucibus tellus, sed gravida lacus. Vivamus eu diam eros. Aliquam et sapien eget arcu rhoncus scelerisque.
                        </p>
                    </div>
                <div>
                   <div>
                    <form action="" method="post">
                    <div class="tab-pane fade" id="account-tab" role="tabpanel" aria-labelledby="account-nav">
                        <h4>Account Details</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <label>First Name</label>
                                <input class="form-control" type="text" placeholder="First_Name" name="user_firstname" value="<?php echo $row['user_firstname'] ?>"><br>
                            </div>
                            <div class="col-md-6">
                            <label>Last Name</label>
                                <input class="form-control" type="text" placeholder="Last_Name" name="user_lastname" value="<?php echo $row['user_lastname'] ?>"><br>
                            </div>
                            <div class="col-md-6">
                            <label>Phone</label>
                                <input class="form-control" type="text" placeholder="Mobile" name="user_phone" value="<?php echo $row['user_phone'] ?>"><br>
                            </div>
                            <div class=" col-md-6">
                            <label>Email</label>
                                <input class="form-control" type="text" placeholder="Email" name="user_email"  value="<?php echo $row['user_email'] ?>"><br>
                            </div>
                            <div class=" col-md-6">
                            <label>Password</label>
                                <input class="form-control" type="text" placeholder="<?php echo $row['user_password'] ?>" name="user_password"  value=""><br>
                            </div>
                            <div class=" col-md-6">
                            <label>Confirm Password</label>
                                <input class="form-control" type="text" placeholder="" name="user_password2"  value=""><br>
                            </div>
                            <div class=" col-md-6">
                            <label>Address 1</label>
                                <input class="form-control" type="text" placeholder="Address" name="user_address1" value="<?php echo $row['user_address1'] ?>"><br>
                            </div>
                            <div class=" col-md-6">
                            <label>Address 2</label>
                                <input class="form-control" type="text" placeholder="Address" name="user_address2" value="<?php echo $row['user_address2'] ?>"><br>
                            </div>
                            <div class=" col-md-4">
                            <label>Country</label>
                                <input class="form-control" type="text" placeholder="Address" name="user_country" value="<?php echo $row['user_country'] ?>"><br>
                            </div>
                            <div class=" col-md-4">
                            <label>City</label>
                                <input class="form-control" type="text" placeholder="Address" name="user_city" value="<?php echo $row['user_city'] ?>"><br>
                            </div>
                            <div class=" col-md-4">
                            <label>Zip code</label>
                                <input class="form-control" type="text" placeholder="Address" name="user_zip_code" value="<?php echo $row['user_zip_code'] ?>"><br>
                            </div>
                                           
                            <div class=" col-md-12">
                                <br>
                                <button class="btn btn-success" type="submit" name="submit">Update Account</button>
                            </div>
                        </div>
                    </div>
</form>
                </div>
</div>
            </div>
        </div>
    </div>
</div>
<!-- My Account End -->



<!-- Back to Top -->
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="lib/easing/easing.min.js"></script>
<script src="lib/slick/slick.min.js"></script>

<!-- Template Javascript -->
<script src="js/main.js"></script>

<?php include('includes/footer.php'); ?>